CREATE DATABASE records;
\c records

CREATE TABLE workshops
(
  id serial PRIMARY KEY,
  attendee text NOT NULL,
  workshop text NOT NULL,
  UNIQUE (attendee, workshop)
);

INSERT INTO workshops (attendee, workshop) VALUES
('Ahmed Abdelali', 'DevOps 101'),
 ('Ann Frank', 'Modern Javascript'),
 ('Ann Mulkern', 'Self-Driving Cars'),
 ('Clara Weick', 'DevOps 101'),
 ('James Archer', 'Docker Container Fundamentals'),
 ('Linda Park', 'Machine Learning'),
 ('Lucy Smith', 'Modern Javascript'),
 ('Roz Billingsley', 'MongoDB'),
 ('Samantha Eggert', 'React Fundamentals'),
 ('Tim Smith', 'Self-Driving Cars');


GRANT SELECT, INSERT ON workshops to records;
GRANT USAGE on workshops_id_seq to records;
