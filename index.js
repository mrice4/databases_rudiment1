const express = require("express");
const bodyParser = require("body-parser");
const app = express();

app.set("port", 8080);
app.use(bodyParser.json({type:"application/json"}));
app.use(bodyParser.urlencoded({extended: true}));

const Pool = require("pg").Pool;
const config = {
  host: "localhost",
  user: "records",
  password: "Th3W0rmTurn5",
  database: "records"
};

const pool = new Pool(config);

app.get("/api", async(req, res) => {
  try{
     if (req.query.workshop) {
      const workshop = req.query.workshop;
      const template = "SELECT attendee FROM workshops WHERE workshop = $1";
      const response = await pool.query(template, [workshop]);
      if(response.rowCount == 0){
        throw "workshop not found";
      } else{
        const attendeeList = response.rows.map(function(item) {
          return item.attendee;
        });
        res.json({attendees: attendeeList});
        }
    } else {
      const template = "SELECT DISTINCT workshop FROM workshops";
      const response = await pool.query(template, []);
      const workshopList = response.rows.map(function(item) {
        return item.workshop;
      });
      res.json({workshops: workshopList});
    }
  } catch(err){
    console.error("Error Running Query " + err);
    res.json({error: err});
  }
});

app.post("/api", async(req, res) => {
  const attendee = req.body.attendee;
  const workshop = req.body.workshop;
  try {
    const template = "INSERT INTO workshops (attendee, workshop) VALUES ($1, $2)";
    const response = await pool.query(template, [attendee, workshop]);
    res.json({results: {attendee: attendee, workshop: workshop}});
  } catch (err) {
    console.error("Error Running Post Request: " + err + " code: " + err.code);
    //res.json({error: err});
    if (err.code == 23502) {
      res.json({error: 'parameters not given'});
    } else if (err.code == 23505) {
      res.json({error: 'attendee already enrolled'});
    } else {
      res.json({error: err, message: 'Unforseen error occured, contact builder'});
    }
  }
});

app.listen(app.get("port"), () => {
  console.log(`Find the server at http://localhost:${app.get("port")}`);
});
